From: X0RW3LL <x0rw3ll@gmail.com>
Date: Mon, 30 Sep 2024 20:02:00 +0300
Subject: Language and Python 3.12 fixes

* Fix Python 3.12 SyntaxWarning
* Various typo and grammar fixes
* masky/ui/main.py: future-proof ASCII art by justifying spaces
  following version so as to avoid manually doing that in future
  double-digit version bumps

Forwarded: https://github.com/Z4kSec/Masky/pull/7
---
 masky/lib/cert/auth.py |  4 ++--
 masky/lib/results.py   |  4 ++--
 masky/lib/smb.py       | 34 +++++++++++++++++-----------------
 masky/ui/console.py    |  4 ++--
 masky/ui/main.py       |  6 +++---
 masky/ui/options.py    |  2 +-
 6 files changed, 27 insertions(+), 27 deletions(-)

diff --git a/masky/lib/cert/auth.py b/masky/lib/cert/auth.py
index 67b3aea..c9cd242 100644
--- a/masky/lib/cert/auth.py
+++ b/masky/lib/cert/auth.py
@@ -149,7 +149,7 @@ class Authenticate:
                 try:
                     self.dc_ip = socket.gethostbyname(self.dc_domain)
                 except:
-                    err_msg = "The provided DC IP is invalid / not set and the domain could not been resolved"
+                    err_msg = "The provided DC IP is invalid/unset and the domain could not be resolved"
                     logger.error(err_msg)
                     self.tracker.last_error_msg = err_msg
                     return False
@@ -425,7 +425,7 @@ class Authenticate:
 
             if not is_key_credential:
                 logger.result(
-                    f"Gathered NT hash for the user '{domain}\{username}': {nt_hash}"
+                    fr"Gathered NT hash for the user '{domain}\{username}': {nt_hash}"
                 )
                 self.user.lm_hash = lm_hash
                 self.user.nt_hash = nt_hash
diff --git a/masky/lib/results.py b/masky/lib/results.py
index 498d8a2..83adf59 100644
--- a/masky/lib/results.py
+++ b/masky/lib/results.py
@@ -34,7 +34,7 @@ class MaskyResults:
                 users += user.split("'")[1] + " "
             if users:
                 logger.warning(
-                    f"Fail to retrieve a PEM from the provided template name for the following users: {users}"
+                    f"Failed to retrieve a PEM from the provided template name for the following users: {users}"
                 )
 
         if self.json_data:
@@ -57,7 +57,7 @@ class MaskyResults:
             err_msg = f"The Masky agent execution failed due to the following errors:\n{self.errors}"
             logger.debug(err_msg)
             self.tracker.last_error_msg = (
-                f"The Masky agent execution failed, probably empty certificates"
+                f"The Masky agent execution failed, probably due to empty certificates"
             )
         else:
             self.tracker.last_error_msg = (
diff --git a/masky/lib/smb.py b/masky/lib/smb.py
index 2ec7070..199f214 100644
--- a/masky/lib/smb.py
+++ b/masky/lib/smb.py
@@ -97,18 +97,18 @@ class Smb:
             self.__command = f'{self.__masky_remote_path} /ca:"{ca}" /template:"{template}" /output:"{self.__results_remote_path}" /debug:"{self.__errors_remote_path}"'
             self.__upload_masky(target)
             logger.debug(
-                f"Masky agent was successfuly uploaded in: '{self.__masky_remote_path}'"
+                f"Masky agent was successfully uploaded in: '{self.__masky_remote_path}'"
             )
         except Exception as e:
             err_msg = None
             if "STATUS_ACCESS_DENIED" in str(e):
-                err_msg = f"The user {self.__domain}\{self.__username} is not local administrator on this system"
+                err_msg = fr"The user {self.__domain}\{self.__username} is not a local administrator on this system"
                 logger.warn(err_msg)
             elif "STATUS_LOGON_FAILURE" in str(e):
-                err_msg = f"The provided credentials for the user '{self.__domain}\{self.__username}' are invalids or the user does not exist"
+                err_msg = fr"The provided credentials for the user '{self.__domain}\{self.__username}' are invalid or the user does not exist"
                 logger.error(err_msg)
             else:
-                err_msg = f"Fail to upload the agent ({str(e)})"
+                err_msg = f"Failed to upload the agent ({str(e)})"
                 logger.error(err_msg)
             self.__tracker.last_error_msg = err_msg
             raise Exception
@@ -118,14 +118,14 @@ class Smb:
             if self.__stealth:
                 self.__edit_svc()
                 logger.debug(
-                    f"The service '{self.__svc_name}' was successfuly modified"
+                    f"The service '{self.__svc_name}' was successfully modified"
                 )
             else:
                 self.__create_svc()
-                logger.debug(f"The service '{self.__svc_name}' was successfuly created")
+                logger.debug(f"The service '{self.__svc_name}' was successfully created")
         except Exception as e:
             err_msg = (
-                f"Fail to edit or create the '{self.__svc_name}' service via DCERPC"
+                f"Failed to edit or create the '{self.__svc_name}' service via DCERPC"
             )
             logger.error(err_msg)
             self.__tracker.last_error_msg = err_msg
@@ -208,7 +208,7 @@ class Smb:
         except:
             self.__tracker.files_cleaning_success = False
             logger.warn(
-                f"Fail to remove Masky agent located in: {self.__masky_remote_path}"
+                f"Failed to remove Masky agent located in: {self.__masky_remote_path}"
             )
 
         if self.__file_args:
@@ -217,7 +217,7 @@ class Smb:
             except:
                 self.__tracker.files_cleaning_success = False
                 logger.warn(
-                    f"Fail to remove Masky agent arguments file located in: {self.__args_path}"
+                    f"Failed to remove Masky agent arguments file located in: {self.__args_path}"
                 )
 
         smbclient.close()
@@ -258,7 +258,7 @@ class Smb:
         except:
             self.__tracker.files_cleaning_success = False
             logger.warn(
-                f"Fail to remove Masky agent output file located in: {self.__results_remote_path}"
+                f"Failed to remove Masky agent output file located in: {self.__results_remote_path}"
             )
 
         try:
@@ -268,7 +268,7 @@ class Smb:
                 rslt.parse_agent_errors,
             )
             if rslt.errors:
-                err_msg = f"The Masky agent execution failed, enable the debugging to display the stacktrace"
+                err_msg = f"The Masky agent execution failed, enable debugging to display the stacktrace"
                 logger.error(err_msg)
         except:
             logger.warn("No Masky agent error file was downloaded")
@@ -277,7 +277,7 @@ class Smb:
         except:
             self.__tracker.files_cleaning_success = False
             logger.warn(
-                f"Fail to remove Masky agent error file located in: {self.__errors_remote_path}"
+                f"Failed to remove Masky agent error file located in: {self.__errors_remote_path}"
             )
 
         if rslt.json_data and len(rslt.json_data) == 0:
@@ -292,7 +292,7 @@ class Smb:
         return rslt
 
     def __init_rpc(self, target_host):
-        np_bind = f"ncacn_np:{target_host}[\pipe\svcctl]"
+        np_bind = fr"ncacn_np:{target_host}[\pipe\svcctl]"
         self.__rpc_con = transport.DCERPCTransportFactory(np_bind)
         self.__rpc_con.set_dport(self.__port)
         self.__rpc_con.setRemoteHost(target_host)
@@ -382,7 +382,7 @@ class Smb:
         except Exception as e:
             self.__tracker.svc_cleaning_success = False
             logger.warn(
-                f"Fail to revert '{self.__svc_name}' service binary path ({str(e)}])"
+                f"Failed to revert '{self.__svc_name}' service binary path ({str(e)}])"
             )
 
     def __remove_svc(self):
@@ -394,7 +394,7 @@ class Smb:
             )
         except Exception as e:
             self.__tracker.svc_cleaning_success = False
-            logger.warn(f"Fail to remove '{self.__svc_name}' service ({str(e)}])")
+            logger.warn(f"Failed to remove '{self.__svc_name}' service ({str(e)}])")
 
     def __clean(self, target_host):
         try:
@@ -415,7 +415,7 @@ class Smb:
             except Exception as e:
                 self.__tracker.svc_cleaning_success = False
                 logger.warning(
-                    f"An unknown error occured while trying to revert or remove '{self.__svc_name}' ({str(e)})"
+                    f"An unknown error occurred while trying to revert or remove '{self.__svc_name}' ({str(e)})"
                 )
         try:
             scmr.hRControlService(
@@ -428,4 +428,4 @@ class Smb:
             self.__remove_masky(target_host)
         except Exception as e:
             self.__tracker.files_cleaning_success = False
-            logger.warn(f"Fail to remove Masky related files on the target ({str(e)}")
+            logger.warn(f"Failed to remove Masky related files on the target ({str(e)}")
diff --git a/masky/ui/console.py b/masky/ui/console.py
index 048bf0f..f02c974 100644
--- a/masky/ui/console.py
+++ b/masky/ui/console.py
@@ -88,7 +88,7 @@ class Console:
         cli_parser = get_cli_args()
         self.__opts = Options(cli_parser)
         if not self.__opts.process():
-            logger.error("The provided options are invalids")
+            logger.error("The provided options are invalid")
             return False
         self.__run()
         return True
@@ -102,7 +102,7 @@ class Console:
                 try:
                     self.__process_results(rslt)
                 except Exception as e:
-                    logger.warn(f"Fail to process results ({str(e)})")
+                    logger.warn(f"Failed to process results ({str(e)})")
         except KeyboardInterrupt:
             logger.warn(
                 "Multiple interruption signals were triggered, Masky is forced to exit without properly cleaning currently processed servers"
diff --git a/masky/ui/main.py b/masky/ui/main.py
index 6a72b97..d8a465a 100644
--- a/masky/ui/main.py
+++ b/masky/ui/main.py
@@ -10,13 +10,13 @@ logger = logging.getLogger("masky")
 
 def print_banner():
     print(
-        f"""
+        fr"""
   __  __           _          
  |  \/  | __ _ ___| | ___   _ 
  | |\/| |/ _` / __| |/ / | | |
  | |  | | (_| \__ \   <| |_| |
- |_|  |_|\__,_|___/_|\_\\__,  |
-  v{VERSION}                 |___/ 
+ |_|  |_|\__,_|___/_|\_\\__, |
+  v{VERSION:22s}|___/
     """
     )
 
diff --git a/masky/ui/options.py b/masky/ui/options.py
index 08be7d9..37fc654 100644
--- a/masky/ui/options.py
+++ b/masky/ui/options.py
@@ -161,7 +161,7 @@ def get_cli_args():
         "-ca",
         "--certificate-authority",
         action="store",
-        help="Certificate Authority Name (SERVER\CA_NAME)",
+        help=r"Certificate Authority Name (SERVER\CA_NAME)",
         required=True,
     )
     group_connect.add_argument(
